<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Pdf;
use Storage;
use Str;

class ProductController extends Controller
{

    public function index()
    {
        $product = Product::latest()->paginate(5);
        return view('admin.product.index', compact('product'));
    }

    public function create()
    {
        return view('admin.product.create');
    }

    public function store(Request $request)
    {
        //validate form
        $this->validate($request, [
            'name' => 'required|min:5',
            'image' => 'required|image|mimes:jpeg,jpg,png|max:2048',
            'desc' => 'required|min:10',
        ]);

        $product = new Product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->slug = Str::slug($product->name, '-');
        $product->desc = $request->desc;
        // upload image
        $image = $request->file('image');
        $image->storeAs('public/products', $image->hashName());
        $product->image = $image->hashName();
        $product->save();
        toast('data have been saved!', 'success');
        return redirect()->route('product.index');

    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view('admin.product.show', compact('product'));
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);
        return view('admin.product.edit', compact('product'));

    }
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required|min:5',
            'image' => 'required|image|mimes:jpeg,jpg,png|max:2048',
            'desc' => 'required|min:10',
        ]);

        $product = Product::findOrFail($id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->slug = Str::slug($product->name, '-');
        $product->desc = $request->desc;
        // upload image
        $image = $request->file('image');
        $image->storeAs('public/products', $image->hashName());
        //delete old image
        Storage::delete('public/products/' . $product->image);

        $product->image = $image->hashName();
        $product->save();
        toast('data have been edited!', 'success');
        return redirect()->route('product.index');

    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        Storage::delete('public/products/' . $product->image);
        $product->delete();
        toast('data have been deleted!', 'success');
        return redirect()->route('product.index');
    }

    // export pdf
    public function exportPdf()
    {
        $product = Product::latest()->get();

        $data = [
            'title' => 'Data Product',
            'date' => date('m/d/Y'),
            'product' => $product,
        ];

        $pdf = PDF::loadView('admin.product.export-pdf', $data)
            ->setPaper('a4', 'portrait');
        return $pdf->stream();
    }

}
