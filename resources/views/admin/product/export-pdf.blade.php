<!DOCTYPE html>
<html>

<head>
    <title>Export Produk</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <center>
        <h1>{{ $title }}</h1>
    </center>
    <p>Tanggal: {{ date('d M Y', strtotime($date)) }}</p>
    <table id="dataTable" class="table">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Produk</th>
                <th>Harga</th>
                <th>Deskripsi</th>
                <th>Foto</th>
            </tr>
        </thead>
        <tbody>
            @php $no = 1; @endphp
            @forelse ($product as $data)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $data->name }}</td>
                    <td>@currency($data->price)</td>
                    <td>{{ Str::limit($data->desc, 25) }}</td>
                    <td>

                        <img src="{{ storage_path('app/public/products/' . $data->image) }}" class="rounded"
                            style="width: 100px">
                    </td>

                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center">
                        Data data belum Tersedia.
                    </td>
                </tr>
            @endforelse
        </tbody>
    </table>

</body>

</html>
