@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="float-start">
                            {{ __('Product') }}
                        </div>
                        <div class="float-end">
                            <a href="{{ route('product.index') }}" class="btn btn-sm btn-outline-primary">Kembali</a>
                        </div>
                    </div>

                    <div class="card-body">
                        <img src="{{ asset('storage/products/' . $product->image) }}" class="w-100 rounded">
                        <hr>
                        <h4>{{ $product->name }}</h4>
                        <p class="tmt-3">
                            <strong>Harga</strong> : @currency($product->price)
                        </p>
                        <strong>Deskripsi :</strong>
                        <p class="tmt-3" style="text-align: justify">
                            {!! $product->desc !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
